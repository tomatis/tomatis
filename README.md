From comprehensive autism assessment to one-on-one regular consultation, we make sure that we give you our full support all throughout the way. We at Tomatis Australia are committed to helping clients in dealing with ADHD, ADD, Aspergers Syndrome, Autism, Learning Difficulties, Speech Delay Difficulties, Auditory Processing, Depression, Stress and Personal Development.

Website: https://tomatis.com.au/
